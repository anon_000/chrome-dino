import 'dart:math';

import 'package:chrome_dino_game/config/enums.dart';
import 'package:chrome_dino_game/config/helpers.dart';
import 'package:chrome_dino_game/config/shared_preferences_helper.dart';
import 'package:chrome_dino_game/game_objects/cactus.dart';
import 'package:chrome_dino_game/game_objects/cloud.dart';
import 'package:chrome_dino_game/game_objects/dino.dart';
import 'package:chrome_dino_game/game_objects/ground.dart';
import 'package:chrome_dino_game/model/game_object.dart';
import 'package:chrome_dino_game/widgets/game_over_widget.dart';
import 'package:chrome_dino_game/widgets/score_board.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

///
/// Created by Auro (aurosmruti@smarttersstudio.com) on 27/12/20 at 1:29 PM
///

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen>
    with SingleTickerProviderStateMixin {
  AnimationController animController;
  Dino dino = Dino();
  double distanceRan = 0;
  double runVelocity = 30;
  Duration lastUpdateCall = Duration();

  List<Cactus> cacti = [];

  List<Ground> ground = [];

  List<Cloud> clouds = [];

  int get score => distanceRan.floor();

  _initialize() {
    cacti = [Cactus(location: Offset(200, 0))];
    clouds = [
      Cloud(location: Offset(100, 20)),
      Cloud(location: Offset(200, 10)),
      Cloud(location: Offset(350, -10)),
    ];
    ground = [
      Ground(location: Offset(0, 0)),
      Ground(location: Offset(groundSprite.width / 10, 0))
    ];
    distanceRan = 0;
    lastUpdateCall = Duration();
  }

  _reset() {
    _initialize();
    dino.reset();
    animController.forward();
  }

  _update() {
    dino.update(lastUpdateCall, animController.lastElapsedDuration);
    double elapsedTimeSeconds =
        (animController.lastElapsedDuration - lastUpdateCall).inMilliseconds /
            1000;
    distanceRan += runVelocity * elapsedTimeSeconds;
    _cactusUpdate();
    _groundUpdate();
    _cloudUpdate();
    lastUpdateCall = animController.lastElapsedDuration;
    setState(() {});
  }

  _cactusUpdate() {
    Rect dinoRect = dino.getRect(Get.size, distanceRan);
    for (Cactus cactus in cacti) {
      Rect obstacleRect = cactus.getRect(Get.size, distanceRan);
      if (dinoRect.overlaps(obstacleRect.deflate(25))) {
        _die();
      }

      if (obstacleRect.right < 0) {
        setState(() {
          cacti.remove(cactus);
          cacti.add(Cactus(
              location: Offset(distanceRan + Random().nextInt(100) + 50, 0)));
        });
      }
    }
  }

  _groundUpdate() {
    for (Ground g in ground) {
      Rect groundRect = g.getRect(Get.size, distanceRan);
      if (groundRect.right < 0) {
        setState(() {
          ground.remove(g);
          ground.add(Ground(
              location: Offset((ground.last.location.dx + 2399) / 10, 0)));
        });
      }
    }
  }

  _cloudUpdate() {
    for (Cloud cloud in clouds) {
      Rect cloudRect = cloud.getRect(Get.size, distanceRan);
      if (cloudRect.right < 0) {
        setState(() {
          clouds.remove(cloud);
          clouds.add(Cloud(
              location: Offset(
                  clouds.last.location.dx + Random().nextInt(100) + 50,
                  Random().nextInt(40) - 20.0)));
        });
      }
    }
  }

  void _die() {
    if (score > SharedPreferenceHelper.score) {
      SharedPreferenceHelper.storeScore(score);
    }
    setState(() {
      animController.stop();
      dino.die();
    });
  }

  @override
  void initState() {
    super.initState();
    _initialize();
    animController =
        AnimationController(vsync: this, duration: Duration(days: 1));
    animController.addListener(_update);
    animController.forward();
  }

  @override
  void dispose() {
    animController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size size = Get.size;
    List<Widget> children = [];

    for (GameObject obj in [...clouds, ...ground, ...cacti, dino]) {
      Rect objRect = obj.getRect(size, distanceRan);
      children.add(AnimatedBuilder(
        animation: animController,
        builder: (ctx, w) => Positioned(
            top: objRect.top,
            left: objRect.left,
            width: objRect.width,
            height: objRect.height,
            child: obj.render()),
      ));
    }

    return Scaffold(
      appBar: AppBar(
        title: Text("Chrome Dino"),
      ),
      body: GestureDetector(
        onTap: () {
          dino.jump();
        },
        behavior: HitTestBehavior.translucent,
        child: Stack(
          alignment: Alignment.center,
          children: [
            ...children,
            Positioned(top: 20, right: 20, child: ScoreBoard(score)),
            AnimatedPositioned(
                duration: Duration(milliseconds: 300),
                bottom: dino.state == DinoState.dead ? 50 : -200,
                right: 0,
                left: 0,
                child: GameOverWidget(
                  onTap: _reset,
                ))
          ],
        ),
      ),
    );
  }
}
