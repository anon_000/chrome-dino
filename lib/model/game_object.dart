import 'package:flutter/material.dart';

///
/// Created by Auro (aurosmruti@smarttersstudio.com) on 27/12/20 at 1:06 PM
///

abstract class GameObject {
  Widget render();

  Rect getRect(Size screenSize, double runDistance);

  void update(Duration lastUpdate, Duration timeElapsed);
}
