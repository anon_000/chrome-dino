import 'dart:math';
import 'package:chrome_dino_game/config/constants.dart';
import 'package:chrome_dino_game/config/helpers.dart';
import 'package:chrome_dino_game/model/game_object.dart';
import 'package:chrome_dino_game/model/sprite.dart';
import 'package:flutter/material.dart';

///
/// Created by Auro (aurosmruti@smarttersstudio.com) on 27/12/20 at 1:12 PM
///

class Cactus extends GameObject {
  final Sprite sprite;
  final Offset location;

  Cactus({this.location}) : sprite = cacti[Random().nextInt(cacti.length)];

  @override
  Rect getRect(Size screenSize, double runDistance) {
    return Rect.fromLTWH((location.dx - runDistance) * WORLD_TO_PIXEL,
        screenSize.height / 1.7 - sprite.height, sprite.width, sprite.height);
  }

  @override
  Widget render() {
    return Image.asset(sprite.path);
  }

  @override
  void update(Duration lastUpdate, Duration timeElapsed) {}
}
