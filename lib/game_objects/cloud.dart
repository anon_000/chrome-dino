import 'package:chrome_dino_game/config/constants.dart';
import 'package:chrome_dino_game/config/helpers.dart';
import 'package:chrome_dino_game/model/game_object.dart';
import 'package:chrome_dino_game/model/sprite.dart';
import 'package:flutter/material.dart';

///
/// Created by Auro (aurosmruti@smarttersstudio.com) on 27/12/20 at 1:12 PM
///

class Cloud extends GameObject {
  final Sprite sprite;

  final Offset location;

  Cloud({this.location}) : sprite = cloudSprite;

  @override
  Rect getRect(Size screenSize, double runDistance) {
    return Rect.fromLTWH(
        (location.dx - runDistance) * WORLD_TO_PIXEL / 5,
        screenSize.height / 3.4 - sprite.height - location.dy,
        sprite.width,
        sprite.height);
  }

  @override
  Widget render() {
    return Image.asset(sprite.path);
  }

  @override
  void update(Duration lastUpdate, Duration timeElapsed) {}
}
