import 'package:chrome_dino_game/config/constants.dart';
import 'package:chrome_dino_game/config/enums.dart';
import 'package:chrome_dino_game/config/helpers.dart';
import 'package:chrome_dino_game/model/game_object.dart';
import 'package:chrome_dino_game/model/sprite.dart';
import 'package:flutter/material.dart';

///
/// Created by Auro (aurosmruti@smarttersstudio.com) on 27/12/20 at 1:10 PM
///

class Dino extends GameObject {
  Sprite currentSprite = dino[0];
  double vDisplacement = 0;
  double vVelocity = 0;
  DinoState state = DinoState.running;

  @override
  Rect getRect(Size screenSize, double runDistance) {
    return Rect.fromLTWH(
        screenSize.width / 10,
        screenSize.height / 1.7 - currentSprite.height - vDisplacement,
        currentSprite.width,
        currentSprite.height);
  }

  @override
  Widget render() {
    return Image.asset(currentSprite.path);
  }

  @override
  void update(Duration lastDuration, Duration currentDuration) {
    //print("vDis : $vDisplacement | state $state");
    currentSprite =
        dino[(currentDuration.inMilliseconds / 100).floor() % 2 + 2];
    double timeElapsedInSeconds =
        (currentDuration - lastDuration).inMilliseconds / 1000;
    vDisplacement += vVelocity * timeElapsedInSeconds;
    if (vDisplacement <= 0) {
      vDisplacement = 0;
      vVelocity = 0;
      state = DinoState.running;
    } else {
      vVelocity -= GRAVITY_PPSS * timeElapsedInSeconds;
    }
  }

  void jump() {
    if (state != DinoState.jumping) {
      state = DinoState.jumping;
      vVelocity = 750;
    }
  }

  void die() {
    currentSprite = dino[5];
    state = DinoState.dead;
  }

  void reset() {
    state = DinoState.running;
    vDisplacement = 0;
    vVelocity = 0;
    currentSprite = dino[2];
  }
}
