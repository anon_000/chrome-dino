import 'package:chrome_dino_game/model/sprite.dart';

///
/// Created by Auro (aurosmruti@smarttersstudio.com) on 27/12/20 at 1:39 PM
///

Sprite groundSprite = Sprite()
  ..path = "assets/ground/ground.png"
  ..width = 2399
  ..height = 24;

Sprite cloudSprite = Sprite()
  ..path = "assets/clouds/cloud.png"
  ..width = 92
  ..height = 27;

List<Sprite> dino = [
  Sprite()
    ..path = "assets/dino/dino_1.png"
    ..height = 94
    ..width = 88,
  Sprite()
    ..path = "assets/dino/dino_2.png"
    ..height = 94
    ..width = 88,
  Sprite()
    ..path = "assets/dino/dino_3.png"
    ..height = 94
    ..width = 88,
  Sprite()
    ..path = "assets/dino/dino_4.png"
    ..height = 94
    ..width = 88,
  Sprite()
    ..path = "assets/dino/dino_5.png"
    ..height = 94
    ..width = 88,
  Sprite()
    ..path = "assets/dino/dino_6.png"
    ..height = 94
    ..width = 88,
];

List<Sprite> cacti = [
  Sprite()
    ..path = "assets/obstacles/cacti_group.png"
    ..width = 104
    ..height = 100,
  Sprite()
    ..path = "assets/obstacles/cacti_large_1.png"
    ..width = 104
    ..height = 100,
  Sprite()
    ..path = "assets/obstacles/cacti_large_2.png"
    ..width = 104
    ..height = 100,
  Sprite()
    ..path = "assets/obstacles/cacti_small_1.png"
    ..width = 104
    ..height = 100,
  Sprite()
    ..path = "assets/obstacles/cacti_small_2.png"
    ..width = 104
    ..height = 100,
  Sprite()
    ..path = "assets/obstacles/cacti_small_3.png"
    ..width = 104
    ..height = 100,
];
