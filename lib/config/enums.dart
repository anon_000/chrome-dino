///
/// Created by Auro (aurosmruti@smarttersstudio.com) on 27/12/20 at 1:32 PM
///

enum DinoState {
  jumping,
  running,
  dead,
}
