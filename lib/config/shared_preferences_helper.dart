import 'package:shared_preferences/shared_preferences.dart';

///
/// Created by Auro (aurosmruti@smarttersstudio.com) on 28/12/20 at 12:05 AM
///

class SharedPreferenceHelper {
  static const SCORE_KEY = "score";
  static SharedPreferences preferences;

  static void storeScore(int score) {
    preferences.setInt(SCORE_KEY, score);
  }

  static int get score => preferences.getInt(SCORE_KEY) ?? 0;
}
