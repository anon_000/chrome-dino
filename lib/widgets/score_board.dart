import 'package:chrome_dino_game/config/shared_preferences_helper.dart';
import 'package:flutter/material.dart';

///
/// Created by Auro (aurosmruti@smarttersstudio.com) on 27/12/20 at 11:02 PM
///

class ScoreBoard extends StatelessWidget {
  final int score;

  ScoreBoard(this.score);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 200,
      padding: const EdgeInsets.symmetric(vertical: 15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text("MAX. SCORE : ${SharedPreferenceHelper.score}",
              style: TextStyle(
                fontWeight: FontWeight.w700,
                letterSpacing: 1,
                color: Colors.black.withOpacity(0.6),
              )),
          const SizedBox(height: 8),
          MyDashedLine(),
          const SizedBox(height: 8),
          Text(
            "YOUR SCORE : $score",
            style: TextStyle(
              fontWeight: FontWeight.w700,
              letterSpacing: 1,
              color: Colors.black.withOpacity(0.6),
            ),
          ),
        ],
      ),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          border: Border.all(color: Colors.grey)),
    );
  }
}

class MyDashedLine extends StatelessWidget {
  final double height;
  final Color color;

  const MyDashedLine({this.height = 1, this.color = Colors.grey});

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        final boxWidth = constraints.constrainWidth();
        final dashWidth = 4.0;
        final dashHeight = height;
        final dashCount = (boxWidth / (2 * dashWidth)).floor();
        return Flex(
          children: List.generate(dashCount, (_) {
            return SizedBox(
              width: dashWidth,
              height: dashHeight,
              child: DecoratedBox(
                decoration: BoxDecoration(color: color),
              ),
            );
          }),
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          direction: Axis.horizontal,
        );
      },
    );
  }
}
