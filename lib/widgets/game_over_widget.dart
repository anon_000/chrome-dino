import 'package:flutter/material.dart';

///
/// Created by Auro (aurosmruti@smarttersstudio.com) on 28/12/20 at 12:35 AM
///

class GameOverWidget extends StatelessWidget {
  final VoidCallback onTap;

  GameOverWidget({this.onTap});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 30),
        child: Column(
          children: [
            Text("Game Over",
                style: TextStyle(
                    fontSize: 46,
                    fontFamily: 'arcade',
                    letterSpacing: 8,
                    color: Colors.black.withOpacity(0.7))),
            const SizedBox(height: 12),
            GestureDetector(
                onTap: onTap,
                child: Image.asset('assets/game_over/replay.png')),
          ],
        ),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            border: Border.all(color: Colors.grey, width: 3)),
      ),
    );
  }
}
